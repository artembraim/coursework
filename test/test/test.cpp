﻿// test.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"/*
#include <string>
#include <iostream>
#include <fstream>
#include <conio.h>
#include <iomanip>*/

#include <string>
#include <windows.h>
#include <iostream>
#include <fstream>
#include <conio.h>
#include <iomanip>
#include <io.h>
#include <cstring>


using namespace std;

ofstream fout;
ifstream fin;

struct Employee
{
	char sector[20];
	char fullName[35];

	double volume;
	Employee *next;
	Employee *prev;
};

void Reading(Employee **begin, Employee **end);

int main()
{
	Employee *headList = new Employee;
	Employee *endList = headList;
	//Reading(&headList, &endList);

}

void Reading(Employee **begin, Employee **end)
{
	int k = 0;
	fin.open("Employees.txt");
	*begin = new Employee;
	(*begin)->prev = NULL;
	(*begin)->next = NULL;
	*end = *begin;
	while (!fin.eof()) 
	{
		fin >> (*end)->sector;
		fin >> (*end)->fullName;
		fin >> (*end)->volume;

		(*end)->next = new Employee;
		(*end)->next->prev = *end;
		*end = (*end)->next;
		k++;
	}

	fin.close();
	if (k != 0)
	{
		(*end)->prev->next = NULL;

	}
	else {
		cout << "Нет данных!" << endl;
		system("pause");
		*end = *begin = NULL;
	}
}

void Output(Employee *begin, Employee *end)
{
	system("cls");
	Employee *tmp = begin;
	if (tmp) {
		int count = 1;
		cout << " ____________________________________________________________________________________________________________" << endl;
		cout << "|  №  | Номер поезда   |  Наименование    |    Время прихода    | Время отпраление |   Категория            |" << endl;
		cout << "|_____|________________|__________________|_____________________|__________________|________________________|" << endl;
		while (tmp != end->prev) {
			cout << "|";
			cout << count;
			cout.width(1);
			cout << "    |";
			cout.width(16);
			cout << left << tmp->trainNumber;
			cout << "|";
			cout.width(18);
			cout << left << tmp->name;
			cout << "|";
			cout.width(21);
			cout << left << tmp->arrivalTime;
			cout << "|";
			cout.width(18);
			cout << left << tmp->departureTime;
			cout << "|";
			cout.width(23);
			cout << left << tmp->category;
			cout << " |";
			count++;
			tmp = tmp->next;
			cout << endl;
			cout << "|_____|________________|__________________|_____________________|__________________|________________________|" << endl;
		}
	}
	system("pause");
}


// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
